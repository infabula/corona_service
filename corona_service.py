# corona_service.py
from flask import Flask, render_template, jsonify


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/docters', methods=['GET'])
def get_docters_list():
    # database query (hoofdstuk 2)
    docters = [
        { 'name': 'John'},
        { 'name': 'No' },
        { 'name': 'Bob'},
        { 'name': 'House'}
    ]
    return jsonify(docters)

@app.route('/docters/<docter_name>', methods=['GET'])
def get_docter(docter_name):
    data = { 'name' : docter_name,
             'specialism': 'chirurg'}
    
    return jsonify(data)


@app.route('/patients')
def get_patien_list():
    return 'Hier komt een lijst met patients in json'


@app.route('/age-check')
def age_check():
    data = {'name':"Jeroen",
          'age': 42 }
    
    return render_template("hello.html", **data)



